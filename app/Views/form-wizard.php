    <!doctype html>
    <html lang="en">

    <head>

        <?= $title_meta ?>

        <?= $this->include('partials/head-css') ?>

    </head>

    <?= $this->include('partials/body') ?>

    <!-- Begin page -->
    <div id="layout-wrapper">

        <?= $this->include('partials/menu') ?>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <?= $page_title ?>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title mb-4">Vertical Wizard</h4>

                                    <div id="vertical-example" class="vertical-wizard">
                                    <!-- Seller Details -->
                                    <h3>Personal Details</h3>
                                    <section>
                                        <form>
                                            <div class="row">
											<h4 class="card-title mb-4">Personal Details</h4>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-lastname-input">Last name</label>
                                                        <input type="text" class="form-control" id="basicpill-lastname-input"
														placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address as per Aadhar</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker1">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker1' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>Gender</label>
                                                            <select class="form-select">
                                                                <option selected>Select Gender</option>
                                                                <option value="AE">Male</option>
                                                                <option value="VI">Female </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>Marital Status</label>
                                                            <select class="form-select">
                                                                <option selected>Select Marital Status</option>
                                                                <option value="AE">Single</option>
                                                                <option value="VI">Married </option>
																<option value="VI">Widowed </option>
																<option value="VI">Divorced </option>
                                                            </select>
                                                        </div>
                                                    </div>
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-lastname-input">Religion</label>
                                                        <input type="text" class="form-control" id="basicpill-lastname-input"
														placeholder="Religion">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>Employment Status</label>
                                                            <select class="form-select">
                                                                <option selected>Select Employment Type</option>
                                                                <option value="AE">Self</option>
                                                                <option value="VI">Employed </option>
																<option value="VI">Salaried </option>
																<option value="VI">Housewife </option>
																<option value="VI">Retired </option>
																<option value="VI">NRI </option>
																<option value="VI">Professional </option>
																<option value="VI">Businessman </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                        </form>
                                    </section>

                                    <!-- Company Document -->
                                    <h3>Family Details</h3>
                                    <section>
                                        <form>
                                            <div class="row">
											<h4 class="card-title mb-4">Family Details</h4>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>Title</label>
                                                            <select class="form-select">
                                                                <option selected>Select Title</option>
                                                                <option value="AE">Mr.</option>
                                                                <option value="VI">Mrs. </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-vatno-input">Spouse Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-vatno-input">
                                                    </div>
                                                </div>
												
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-4">
                                        <label>Spouse D.O.B</label>
                                        <div class="input-group" id="datepicker2">
                                            <input type="text" class="form-control" placeholder="dd M, yyyy" data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-servicetax-input">Spouse Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-servicetax-input">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-companyuin-input">Spouse PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-companyuin-input">
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address as per Aadhar</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>

                                    <!-- Bank Details -->
                                    <h3>Beneficiaries</h3>
                                    <section>
                                        <div>
                                            <form class="repeater" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker3">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker3' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nationality">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
												<div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>Your Relationship</label>
                                                            <select class="form-select">
                                                                <option selected>Select Your Relationship</option>
                                                                <option value="AE">Family</option>
                                                                <option value="VI">Relatives </option>
																<option value="VI">Friends </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any ID Proof</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any Address Proof </label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address of Beneficiary</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
											
											<div class="row">
                                                <div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <div data-repeater-list="group-a">
                                        <div data-repeater-item class="row" style="display: none;">
                                            <div class="mb-3 col-lg-6">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" name="untyped-input" class="form-control" />
                                            </div>

                                            <div class="mb-3 col-lg-6">
                                                <label for="email">Email</label>
                                                <input type="email" id="email" class="form-control" />
                                            </div>

                                            <div class="mb-3 col-lg-6">
                                                <label for="subject">Subject</label>
                                                <input type="text" id="subject" class="form-control" />
                                            </div>

                                            <div class="mb-3 col-lg-6">
                                                <label for="resume">Resume</label>
                                                <input type="file" class="form-control-file" id="resume">
                                            </div>

                                            <div class="mb-3 col-lg-6">
                                                <label for="message">Message</label>
                                                <textarea id="message" class="form-control"></textarea>
                                            </div>

                                            <div class="col-lg-2 align-self-center">
                                                <div class="d-grid">
                                                    <input data-repeater-delete type="button" class="btn btn-primary" value="Delete" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <input data-repeater-create type="button" class="btn btn-success mt-3 mt-lg-0" value="Add" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Witness</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker3">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker3' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nationality">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Your Relationship</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Your Relationship">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any ID Proof</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any Address Proof </label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address of Beneficiary</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Executor</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker3">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker3' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nationality">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Your Relationship</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Your Relationship">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any ID Proof</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any Address Proof </label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address of Beneficiary</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Assets Owned</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                                <label class="form-check-label" for="flexSwitchCheckDefault">Immovable Properties</label>
                                            </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault1">
                                                <label class="form-check-label" for="flexSwitchCheckDefault1">Insurance Policies</label>
                                            </div>
                                                </div><div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault2">
                                                <label class="form-check-label" for="flexSwitchCheckDefault2">Vehicles</label>
                                            </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input	" type="checkbox" id="flexSwitchCheckDefault3">
                                                <label class="form-check-label" for="flexSwitchCheckDefault3">Jewellery</label>
                                            </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault4">
                                                <label class="form-check-label" for="flexSwitchCheckDefault4">Provident Fund</label>
                                            </div>
                                                </div><div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault5">
                                                <label class="form-check-label" for="flexSwitchCheckDefault5">Gratuity</label>
                                            </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault6">
                                                <label class="form-check-label" for="flexSwitchCheckDefault6">Bank Accounts</label>
                                            </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault7">
                                                <label class="form-check-label" for="flexSwitchCheckDefault7">Safe Deposit Lockers</label>
                                            </div>
                                                </div><div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault8">
                                                <label class="form-check-label" for="flexSwitchCheckDefault8">Fixed Deposits</label>
                                            </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault9">
                                                <label class="form-check-label" for="flexSwitchCheckDefault9">Demat Accounts</label>
                                            </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault10">
                                                <label class="form-check-label" for="flexSwitchCheckDefault10">Share in Partnership or LLP</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault11">
                                                <label class="form-check-label" for="flexSwitchCheckDefault11">Mutual Funds</label>
                                            </div>
                                                </div>
                                            </div>
                                            <div class="row">
											
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault19">
                                                <label class="form-check-label" for="flexSwitchCheckDefault19">ESOPs</label>
                                            </div>
                                                </div>
												
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault12">
                                                <label class="form-check-label" for="flexSwitchCheckDefault12">Public Provident Fund</label>
                                            </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault13">
                                                <label class="form-check-label" for="flexSwitchCheckDefault13">NSC</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault14">
                                                <label class="form-check-label" for="flexSwitchCheckDefault14">Bond / Debentures</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault15">
                                                <label class="form-check-label" for="flexSwitchCheckDefault15">Digital Assets</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault16">
                                                <label class="form-check-label" for="flexSwitchCheckDefault16">Virtual Currencies</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault18">
                                                <label class="form-check-label" for="flexSwitchCheckDefault18">Loans given</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault17">
                                                <label class="form-check-label" for="flexSwitchCheckDefault17">Others</label>
                                            </div>
                                                </div>
												
                                            </div>
                                        </form>
                                        </div>
										
										<div id="immovableproperties" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title mb-4">Immovable Properties</h4>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Property ID</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Enter ID Provided by any Govt. Authority">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                            <label>Type of Ownership</label>
                                                            <select class="form-select">
                                                                <option selected>Do others have ownership of this asset along with you.</option>
                                                                <option value="AE">Jointly Held</option>
                                                                <option value="VI">Family </option>
																<option value="VI">100% Ownership </option>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Your Holding / Ownership</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="In Percentage">
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                <div class="mb-3">
                                                            <label>Property Type</label>
                                                            <select id="colorselector" class="form-select">
                                                                <option selected>Select Type of Property that you own</option>
                                                                <option value="land">Land</option>
                                                                <option value="apartment">Apartment Building </option>
																<option value="house">House </option>
																<option value="flat">Flat </option>
																<option value="shop">Shop </option>
																<option value="complex">Complex </option>
                                                            </select>
                                                        </div>
                                                        </div>
                                            </div>
											<div class="row colors" id="land" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of Land</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of Land"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the Land </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the land in square feet, acres, cents, guntas, hectres"></div>
                                                </div>
												
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this land generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio1" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio2" value="No">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio3" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio3">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="ifyes" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
                                            
                                            <div class="row colors" id="apartment" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of the building</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of the building"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the Building </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the building in detail"></div>
                                                </div>
												
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Number of units in the Building? </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Number of units in the Building"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this building generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio4" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio4">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio5" value="No">
                                                    <label class="form-check-label" for="inlineRadio5">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio6" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio6">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="revenue" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
												
												<div class="row colors" id="house" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of the house</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of the building"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the house </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the building in detail"></div>
                                                </div>

												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this house generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio7" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio7">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio8" value="No">
                                                    <label class="form-check-label" for="inlineRadio8">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio9" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio9">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="house_revenue" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
												
												<div class="row colors" id="flat" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of the flat</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of the building"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the flat </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the building in detail"></div>
                                                </div>

												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this flat generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio10" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio10">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio11" value="No">
                                                    <label class="form-check-label" for="inlineRadio11">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio12" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio12">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="flat_revenue" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
												
												<div class="row colors" id="shop" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of the shop</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of the building"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the shop </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the building in detail"></div>
                                                </div>

												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this shop generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio13" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio13">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio14" value="No">
                                                    <label class="form-check-label" for="inlineRadio14">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio15" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio15">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="shop_revenue" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
												
												<div class="row colors" id="complex" style="display:none">
											
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Description of the Complex</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Description of the Complex"></div>
                                                    </div>
                                                
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Size of the Complex </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention the extent of the Complex in detail"></div>
                                                </div>
												
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Number of Units in the Complex? </label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Number of Units in the Complex?"></div>
                                                </div>

												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Is this Complex generating any revenue? If yes, how much per month / per year?</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio16" value="Yes">
                                                    <label class="form-check-label" for="inlineRadio16">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio17" value="No">
                                                    <label class="form-check-label" for="inlineRadio17">No</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio18" value="Maybe">
                                                    <label class="form-check-label" for="inlineRadio18">Maybe</label>
                                                </div>
                                            </div>
                                                </div>
												
												<div class="col-lg-6" id="complex_revenue" style="display: none">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">If yes, how much per month / per year?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="If yes, how much per month / per year?"></div>
                                                </div>
												
												<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2" placeholder="As per Sale Deed"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Landmark</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Mention nearest landmark"></div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Supporting Document</label>
                                                <input class="form-control" type="file" id="formFile" placeholder="Maximum file size is 50 MB">
                                            </div>
                                                </div>
												</div>
                                        </form>
                                        </div>
										
										<div id="insurancepolicies" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title">Insurance Policies</h4>
											<p class="card-title-desc">Detailed list of information on the insurance policies.</p>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Name of Insurer</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name of Insurer">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Sum Assured</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Sum Assured">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Period</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Period">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Insurance Premium</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Insurance Premium">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label class="d-block mb-3">Payment Intervals</label>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio19" value="Monthly">
                                                    <label class="form-check-label" for="inlineRadio19">Monthly</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio20" value="Quarterly">
                                                    <label class="form-check-label" for="inlineRadio20">Quarterly</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio21" value="Half Yearly">
                                                    <label class="form-check-label" for="inlineRadio21">Half Yearly</label>
                                                </div>
												<div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="land_generating" id="inlineRadio22" value="Yearly">
                                                    <label class="form-check-label" for="inlineRadio22">Yearly</label>
                                                </div>
                                            </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nominee Details - Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nominee Details - Name">
                                                    </div>
                                                </div>
												
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Relationship</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Your Relationship">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Bond / Policy</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                
													<div class="col-lg-12">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
										
										<div id="vehicles" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title">Vehicles</h4>
											<p class="card-title-desc">Detailed list of information on the insurance policies.</p>
                                                <div class="col-lg-6">
                                                <div class="mb-3">
                                                            <label>Vehicle Type</label>
                                                            <select id="colorselector" class="form-select">
                                                                <option selected>Select Vehicle Type</option>
                                                                <option value="Moped">Moped</option>
                                                                <option value="Motorbike">Motorbike </option>
																<option value="Cycle">Cycle </option>
																<option value="Auto_three_wheeler">Auto - three wheeler </option>
																<option value="Auto_Four_Wheeler">Auto - Four Wheeler </option>
																<option value="Car">Car </option>
																<option value="Tempo">Tempo </option>
																<option value="Goods_Carriage_Canter">Goods Carriage - Canter</option>
																<option value="Goods_Carriage _Truck">Goods Carriage - Truck</option>
																<option value="Boat">Boat</option>
																<option value="Jet_Ski">Jet Ski</option>
																<option value="Private_Vesel">Private Vesel</option>
																<option value="Shipping_Vessel">Shipping Vessel</option>
																<option value="Aircraft">Aircraft</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Vehicle Number</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="KA 01 AA 1234">
                                                    </div>
                                                </div>
                                            </div>
											
											
                                           
                                            <div class="row">
                                                
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Vehicle Registration Documents Upload</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
										
										<div id="jewellery" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title">Jewellery</h4>
											<p class="card-title-desc">Detailed list of all jewelry that you own.</p>
                                                <div class="col-lg-6">
                                                <div class="mb-3">
                                                            <label>Ornaments Type</label>
                                                            <select id="colorselector" class="form-select">
                                                                <option selected>Select Ornaments Type</option>
                                                                <option value="Gold">Gold</option>
                                                                <option value="Silver">Silver </option>
																<option value="Platinum">Platinum </option>
																<option value="Artificial">Artificial </option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Weight in Grams</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Weight in Grams">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Purchase Bills</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
										
										<div id="providentfund" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title mb-4">Provident Fund</h4>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">UAN Number</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="UAN Number">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Balance as per last review</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Balance as per last review">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload Screenshot</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
										
										<div id="gratuity" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title">Gratuity</h4>
											<p class="card-title-desc">Gratuity due from employer</p>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Gratuity Amount Due</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Gratuity Amount Due">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Gratuity Eligibility Letter from Employer</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
										
										<div id="ESOPs" style="display: none">
                                            <form>
                                            <div class="row">
											<h4 class="card-title">ESOPs</h4>
											<p class="card-title-desc">ESOPs due from employer</p>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Number of Shares qualified for?</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Gratuity Amount Due">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">ESOPs eligibility letter received from employer</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Liabilities</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault19">
                                                <label class="form-check-label" for="flexSwitchCheckDefault19">Home Loan</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault20">
                                                <label class="form-check-label" for="flexSwitchCheckDefault20">Vehicle Loan</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault21">
                                                <label class="form-check-label" for="flexSwitchCheckDefault21">Personal Loan</label>
                                            </div>
                                                </div>
												<div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault22">
                                                <label class="form-check-label" for="flexSwitchCheckDefault22">others</label>
                                            </div>
                                                </div>
                                                
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Charity</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-check form-switch mb-3">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault23">
                                                <label class="form-check-label" for="flexSwitchCheckDefault23">Charity</label>
                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>Bequeath</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker3">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker3' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nationality">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Your Relationship</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Your Relationship">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any ID Proof</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any Address Proof </label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address of Beneficiary</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>
									
									<!-- Bank Details -->
                                    <h3>My Message</h3>
                                    <section>
                                        <div>
                                            <form>
                                            <div class="row">
											<h4 class="card-title mb-4">My Message</h4>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Full Name</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Name as per PAN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                        <label>D.O.B</label>
                                        <div class="input-group" id="datepicker3">
                                            <input type="text" class="form-control" placeholder="dd MM, yyyy" data-date-format="dd MM, yyyy" data-date-container='#datepicker3' data-provide="datepicker" data-date-autoclose="true">

                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Nationality</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Nationality">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">PAN</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="PAN">
                                                    </div>
                                                </div>
                                            </div>
											<div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Present Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Permanent Address</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-firstname-input">Your Relationship</label>
                                                        <input type="text" class="form-control" id="basicpill-firstname-input" placeholder="Your Relationship">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any ID Proof</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                
													<div class="col-lg-6">
                                                    <div class="mb-3">
                                                <label for="formFile" class="form-label">Upload any Address Proof </label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>
                                                </div>
												<div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="basicpill-address-input">Address of Beneficiary</label>
                                                        <textarea id="basicpill-address-input" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </section>

                                    <!-- Confirm Details -->
                                    <h3>Confirm Detail</h3>
                                    <section>
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6">
                                                <div class="text-center">
                                                    <div class="mb-4">
                                                        <i class="mdi mdi-check-circle-outline text-success display-4"></i>
                                                    </div>
                                                    <div>
                                                        <h5>Confirm Detail</h5>
                                                        <p class="text-muted">If several languages coalesce, the grammar of the resulting</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


            <?= $this->include('partials/footer') ?>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <?= $this->include('partials/right-sidebar') ?>

    <?= $this->include('partials/vendor-scripts') ?>

    <!-- jquery step -->
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>

    <!-- form wizard init -->
    <script src="assets/js/pages/form-wizard.init.js"></script>

    <script src="assets/js/app.js"></script>

    </body>

    </html>